# coding: utf-8

lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'gladepay/version'

Gem::Specification.new do |spec|
  spec.name          = 'gladepay'
  spec.version       = Gladepay::VERSION
  spec.authors       = ['Chinaka Light']
  spec.email         = ['light@yottabitconsulting.com']

  spec.summary       = %s(A Gem that simplifies payment with Gladepay APIs.)
  spec.description   = %s(A Gem that simplifies payment with Gladepay APIs.)
  spec.homepage      = 'https://gitlab.com/gladepay-apis/gladepay-ruby'
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.15'
  spec.add_development_dependency 'minitest', '~> 5.0'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'

  # Dependencies
  spec.required_ruby_version = '>= 2.2'
  spec.add_runtime_dependency 'rest-client', '~> 2.0'
end
