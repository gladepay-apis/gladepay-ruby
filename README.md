# Gladepay Ruby

![Build Status](https://gitlab.com/gladepay-apis/gladepay-ruby/badges/develop/build.svg) ![Gem Version](https://badge.fury.io/rb/gladepay.svg)

GladePay is the leading digital payments provider with focus across African countries, dedicated to creating simple payments solution for African businesses. We are focused on building trust between buyers and sellers by providing simple solutions using the payment options (online or offline) preferred with complete security and efficiency.

A RUBY Gem Or library that simplifies payment with Gladepay APIs

To Learn more, visit www.gladepay.com.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'gladepay'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install gladepay

## Usage

```ruby
	
	@initialize =
      {
        'action' => 'initiate',
        'paymentType' => 'card',
        'user' => {
          'firstname' => 'Chinaka',
          'lastname' => 'Light',
          'email' => 'test@gladepay.com',
          'ip' => '192.168.33.10',
          'fingerprint' => 'cccvxbxbxb'
        },
        'card' => {
          'card_no' => '5438898014560229',
          'expiry_month' => '09',
          'expiry_year' => '19',
          'ccv' => '789',
          'pin' => '3310'
        },
        'amount' => '10000',
        'country' => 'NG',
        'currency' => 'NGN'
      }

	@merchant_id = "GP0000001" #Test Data
	@merchant_key = "123456789" 
	live = true #for live server, default false for test server

	# Demp or Test Server Instance
	gladepay = Gladepay.new(@merchant_id, @merchant_key) # Or

	# gladepay = Gladepay.new(@merchant_id, @merchant_key, false)

	#LIVE SERVER Instance
	# gladepay = Gladepay.new(@merchant_id, @merchant_key, true) #for live server
		
	#Get Response
	response = gladepay.card_payment(
    @initialize['user'],
    @initialize['card'],
    @initialize['amount'],
    @initialize['country'],
    @initialize['currency']
  )

  # If OTP is required 
	response = gladepay.validate_otp(response['txnRef'], '12345')

	#Verify Transaction		
	response = gladepay.verify_transaction(response['txnRef'])

  puts response["message"] #Transaction Successful

  #Other methods
  #Get list of all Banks:

  all_banks_response = gladepay.all_banks
	
	#Get list of banks that support account payments:
	supported_banks_response = gladepay.supported_banks_account_payment
	
	#Get Details of a card:
	card_details_response = gladepay.card_details(card_number)
	
	#Get the charges applicable to a card: (first six digit of the card no)
	card_charges_response = gladepay.card_charges(amount, card_no)

	#Get the charges applicable to an account
	account_charges_response = gladepay.account_charges(amount)

	#charge with token
	response = gladepay.charge_with_token(
      user,
      token,
      amount
    )
	
	#Perform Money transfer:
	money_transfer_response = gladepay.money_transfer(amount, bankcode, account_number, 'Mark Silas', 'Narration')
	
	#Verify status of Money transfer:
	verify_money_transfer_response = gladepay.verify_money_transfer(txnRef)

	#Verify Account Name:
	account_name_verification_response = gladepay.verify_account_name(bankcode, account_number)
	
	#Return Values
	All methods return an array.

```


## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://gitlab.com/gladepay-apis/gladepay-ruby. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Gladepay project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://gitlab.com/gladepay-apis/gladepay-ruby/blob/master/CODE_OF_CONDUCT.md).
