require 'test_helper'

class GladepayTest < Minitest::Test
  def setup
    @merchant_id = 'GP0000001'
    @merchant_key = '123456789'
    @initialize =
      {
        'action' => 'initiate',
        'paymentType' => 'card',
        'user' => {
          'firstname' => 'Chinaka',
          'lastname' => 'Light',
          'email' => 'test@gladepay.com',
          'ip' => '192.168.33.10',
          'fingerprint' => 'cccvxbxbxb'
        },
        'card' => {
          'card_no' => '5438898014560229',
          'expiry_month' => '09',
          'expiry_year' => '19',
          'ccv' => '789',
          'pin' => '3310'
        },
        'amount' => '10000',
        'country' => 'NG',
        'currency' => 'NGN'
      }

    @charge_initialize =
      {
        'action' => 'charge',
        'paymentType' => 'account',
        'user' => {
          'firstname' => 'Chinaka',
          'lastname' => 'Light',
          'email' => 'test@gladepay.com',
          'ip' => '192.168.33.10',
          'fingerprint' => 'ddsdschhdghgshghdgshghcx'
        },
        'account' => {
          'accountnumber' => '0690000007',
          'bankcode' => '044'
        },
        'amount' => '10000'
      }

    @token_initialize =
      {
        'action' => 'charge',
        'paymentType' => 'token',
        'token' => '01fae68d805957cc24697c990961a130',
        'user' => {
          'firstname' => 'Chinaka',
          'lastname' => 'Light',
          'email' => 'test@gladepay.com',
          'ip' => '192.168.33.10',
          'fingerprint' => 'cccvxbxbxb'
        },
        'account' => {
          'accountnumber' => '0690000007',
          'bankcode' => '044'
        },
        'amount' => '10000'
      }
  end

  def test_that_it_has_a_version_number
    refute_nil ::Gladepay::VERSION
    puts "TEST DATA\n", @initialize, "\n"
  end

  def test_that_glade_instance_is_test_mode
    gladepay = Gladepay.new(@merchant_id, @merchant_key)
    refute_nil gladepay
    assert_equal gladepay.current_base_url, 'https://demo.api.gladepay.com'
  end

  def test_that_instance_is_in_live_mode
    gladepay = Gladepay.new(@merchant_id, @merchant_key, true)

    refute_nil gladepay

    assert_equal gladepay.current_base_url, 'https://api.gladepay.com'
  end

  def test_that_glade_card_payment_initilized_properly
    gladepay = Gladepay.new(@merchant_id, @merchant_key)
    refute_nil gladepay

    response = gladepay.card_payment(
      @initialize['user'],
      @initialize['card'],
      @initialize['amount'],
      @initialize['country'],
      @initialize['currency']
    )

    puts "RESPONSE: #{response}", "\n"
    assert(response['status'] == 200 || response['status'] == 202 || response['status'] == 'error' || response['status'] == 500)
  end

  def test_that_glade_validate_otp_code
    gladepay = Gladepay.new(@merchant_id, @merchant_key)

    response = gladepay.card_payment(
      @initialize['user'],
      @initialize['card'],
      @initialize['amount'],
      @initialize['country'],
      @initialize['currency']
    )

    puts "VALIDATE OTP USING TRANSACTION_REFERENCE #{response['txnRef']}"
    response = gladepay.validate_otp(response['txnRef'], '12345')
    puts response
    assert(response['status'] == 200 || response['status'] == 202 || response['status'] == 104)
  end

  def test_g_validation_code
    gladepay = Gladepay.new(@merchant_id, @merchant_key)

    response = gladepay.card_payment(
      @initialize['user'],
      @initialize['card'],
      @initialize['amount'],
      @initialize['country'],
      @initialize['currency']
    )

    response = gladepay.validate_otp(response['txnRef'], '12345')
    puts response
    assert(response['status'] == 200 || response['status'] == 201 || response['status'] == 104)

    puts "VERIFYING USING TRANSACTION_REFERENCE #{response['txnRef']}"
    response = gladepay.verify_transaction(response['txnRef'])

    puts response
    assert(response['status'] == 200 || response['status'] == 104)
  end

  def test_banks
    gladepay = Gladepay.new(@merchant_id, @merchant_key)
    all_banks_response = gladepay.all_banks

    refute_nil all_banks_response

    # assert all_banks_response['status'] == 200 || response['status'] == 202

    supported_banks_response = gladepay.supported_banks_account_payment
    refute_nil supported_banks_response
  end

  def test_banks_and_charge_with_token
    gladepay = Gladepay.new(@merchant_id, @merchant_key)
    all_banks_response = gladepay.all_banks

    refute_nil all_banks_response
    puts 'ALL BANKS', all_banks_response

    # TODO: test for non empty array assertion all_banks_response['status'] == 200 || all_banks_response['status'] == 202

    supported_banks_response = gladepay.supported_banks_account_payment
    refute_nil supported_banks_response
    puts 'ALL SUPPORTED BANKS', supported_banks_response
    response = gladepay.charge_with_token(
      @token_initialize['user'],
      @token_initialize['token'],
      @token_initialize['amount']
    )

    puts 'CHARGE WITH TOKEN RESPONSE'
    refute_nil response
    puts response
    # assert response['status'] == 200 || response['status'] == 202
  end

  def test_account_payment
    gladepay = Gladepay.new(@merchant_id, @merchant_key)
    account_payment_response = gladepay.account_payment(@charge_initialize['user'], @charge_initialize['account'], @initialize['amount'])
    refute_nil account_payment_response
    puts 'Account Payment RESPONSE ', account_payment_response
    assert account_payment_response['status'] == 200 || account_payment_response['status'] == 202 || account_payment_response['status'] == 500

    card_details_response = gladepay.card_details('1234567898')
    refute_nil card_details_response
    puts 'Card Details', card_details_response
    # assert card_details_response['status'] == 200 || card_details_response['status'] == 202
  end

  def test_card_charges
    gladepay = Gladepay.new(@merchant_id, @merchant_key)
    test_card_charges_response = gladepay.card_charges(@initialize['amount'], '543889')
    refute_nil gladepay
    puts 'CARD CHARGES RESPONSE', test_card_charges_response
    refute_nil test_card_charges_response
    # assert test_card_charges_response['status'] == 200 || test_card_charges_response['status'] == 202
  end

  def test_account_name_verification
    gladepay = Gladepay.new(@merchant_id, @merchant_key)
    test_account_name_verification_response = gladepay.verify_account_name(@charge_initialize['account']['bankcode'], @charge_initialize['account']['accountnumber'])
    puts 'ACCOUNT NAME VERIFICATION', test_account_name_verification_response
    refute_nil test_account_name_verification_response
    # assert test_account_name_verification_response['status'] == 200 || test_account_name_verification_response['status'] == 202
  end

  def test_account_charges
    gladepay = Gladepay.new(@merchant_id, @merchant_key)
    puts 'NON-CARD OR ACCOUNT CHARGES BEGIN'
    test_account_charges_response = gladepay.account_charges(@initialize['amount'])
    puts 'NON-CARD OR ACCOUNT CHARGES RESPONSE', test_account_charges_response
    refute_nil test_account_charges_response
    assert_kind_of Numeric, test_account_charges_response
  end

  def test_transfer
    gladepay = Gladepay.new(@merchant_id, @merchant_key)
    test_money_transfer_response = gladepay.money_transfer(@charge_initialize['amount'], @charge_initialize['account']['bankcode'], @charge_initialize['account']['accountnumber'], 'Mark Silas', 'Narration')
    refute_nil test_money_transfer_response
    puts 'MONEY TRANSFER RESPONSE', test_money_transfer_response
    assert test_money_transfer_response['status'] == 200 || test_money_transfer_response['status'] == 202 || test_money_transfer_response['status'] == 301

    verify_money_transfer_response = gladepay.verify_money_transfer(test_money_transfer_response['txnRef'])
    refute_nil verify_money_transfer_response
    puts 'VERIFYING MONEY TRANSFER RESPONSE', verify_money_transfer_response
    assert verify_money_transfer_response['status'] == 200 || verify_money_transfer_response['status'] == 202 || verify_money_transfer_response['status'] == 203
  end
end
