module Api
  BASE_URL = 'https://api.gladepay.com'.freeze
  DEMO_BASE_URL = 'https://demo.api.gladepay.com'.freeze
end
