# Gladepay Server Error Handler
class GladepayServerError < StandardError
  attr_reader :response
  def initialize(response = nil)
    @response = response
  end
end

class GladepayBadKeyError < StandardError
end
