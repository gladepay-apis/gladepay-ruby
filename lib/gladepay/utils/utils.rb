require 'gladepay/error.rb'

# Utils Module To Handle Server Error
module Utils
  def self.server_error_handler(error_par)
    raise error if error_par.response.nil?

    error = GladepayServerError.new(error_par.response)
    case error_par.response.code
    when 400
      raise error, 'HTTP Code 400: A validation or client side error occurred and the request was not fulfilled. '
    when 401
      raise error, 'HTTP Code 401: The request was not authorized. This can be triggered by passing an invalid secret key in the authorization header or the lack of one'
    when 404
      raise error, 'HTTP Code 404: Request could not be fulfilled as the request resource does not exist.'
    when 500, 501, 502, 503, 504
      raise error, 'Unrecognized Response from Gateway'
    else
      raise error, "HTTP Code #{error_par.response.code}: #{error_par.response.body}"
    end
  end
end
