require 'rest_client'
require 'gladepay/version'
require 'gladepay/error'
require 'gladepay/modules/api'
require 'gladepay/utils/utils'
require 'json'

# => Gladepay Class: \nWrapps Gladepay API Gateway For Ruby
class Gladepay
  include Api

  attr_reader :merchant_id, :merchant_key, :base_url, :live

  def initialize(merchant_id = nil, merchant_key = nil, live = false)
    @merchant_id = if merchant_id.nil?
                     ENV['merchant_id']
                   else
                     merchant_id
                   end

    @merchant_key = if merchant_key.nil?
                      ENV['merchant_key']
                    else
                      merchant_key
                    end

    @live = live

    @base_url = if @live
                  BASE_URL
                else
                  DEMO_BASE_URL
                end

    if @merchant_id.nil?
      raise GladepayBadKeyError, 'No merchant Id supplied and couldn\'t find any in environment variables. Make sure to set merchant Id as an environment variable merchant_id'
    end

    raise GladepayBadKeyError, "Invalid merchant key #{@merchant_id}" unless @merchant_id[0..1] == 'GP'

    if @merchant_key.nil?
      raise GladepayBadKeyError, 'No merchant key supplied and couldn\'t find any in environment variables. Make sure to set the merchant key as an environment variable merchant_key'
    end

    raise GladepayBadKeyError, "Invalid merchant key #{@merchant_key}" unless @merchant_key.to_i.to_s == @merchant_key

    raise GladepayBadKeyError, 'Invalid BaseUrl Must SPECIFY LIVE or DEMO server' if @base_url.nil? && !@base_url.is_a?(TrueClass)
  end

  def current_base_url
    return @base_url
  end

  def card_payment(user_details = {}, card_details = {}, amount, country, currency)
    requests = {
      'user' => user_details,
      'card' => card_details,
      'amount' => amount,
      'country' => country,
      'currency' => currency
    }

    initiate_transaction_response = initiate_transaction(requests)

    if initiate_transaction_response.key? 'status'
      if initiate_transaction_response['status'] == 202
        charge_card_response = charge_card(requests, initiate_transaction_response['txnRef'], initiate_transaction_response['apply_auth'])
        if charge_card_response.key? 'validate'
          respond_ar = {
            'status' => 202,
            'txnRef' => charge_card_response['txnRef'],
            'message' => 'Please require the user to enter an OTP and call `validateOTP` with the `txnRef`'
          }
          return respond_ar
        elsif charge_card_response.key? 'authURL'
          respond_ar = {
            'status' => 202,
            'txnRef' => charge_card_response['txnRef'],
            'authURL' => charge_card_response['authURL'],
            'message' => 'Please load the link contained in `authURL` for the user to validate Payment'
          }
          return respond_ar.to_json
        else
          respond_ar = {
            'status' => 500,
            'message' => 'Unrecognized Response from Gateway.'
          }
          return respond_ar
        end
      else
        respond_ar = {
          'status' => 500,
          'message' => initiate_transaction_response['message']
        }
        return respond_ar
      end

    else
      respond_ar = {
        'status' => 500,
        'message' => 'Unrecognized Response from Gateway.'
      }
      return respond_ar
    end
  end

  def initiate_transaction(request)
    request_data = {
      'action' => 'initiate',
      'paymentType' => 'card',
      'user' => request['user'],
      'card' => request['card'],
      'amount' => request['amount'],
      'country' => request['country'],
      'currency' => request['currency']
    }

    result = call_put_api('payment', request_data)

    return result
  end

  def charge_card(request, txn_ref, auth_type)
    request_data = {
      'action' => 'charge',
      'paymentType' => 'card',
      'user' => request['user'],
      'card' => request['card'],
      'amount' => request['amount'],
      'country' => request['country'],
      'currency' => request['currency'],
      'txnRef' => txn_ref,
      'auth_type' => auth_type
    }

    result = call_put_api('payment', request_data)

    return result
  end

  def charge_with_token(user_details = {}, token, amount)
    request_data = {
      'action' => 'charge',
      'paymentType' => 'token',
      'token' => token,
      'user' => user_details,
      'amount' => amount
    }

    token_response = call_put_api('payment', request_data)

    response = if token_response.key? 'status'
                 if token_response['status'] == 200
                   {
                     'status' => 200,
                     'txnRef' => token_response['txnRef'],
                     'message' => 'Successful Payment'
                   }
                 else
                   {
                     'status' => 500,
                     'message' => 'Error Processing'
                   }
                 end
               else
                 {
                   'status' => 500,
                   'message' => 'Unrecognized Response from Gateway.'
                 }
               end
    return response
  end

  def account_payment(user_details, account_details = {}, amount)
    request_data = {
      'action' => 'charge',
      'paymentType' => 'account',
      'user' => user_details,
      'account' => account_details,
      'amount' => amount
    }

    response = call_put_api('payment', request_data)
    return response
  end

  def all_banks
    request_data = {
      'inquire' => 'banks'
    }
    response = call_put_api('resources', request_data)
    return response
  end

  def supported_banks_account_payment
    request_data = {
      'inquire' => 'supported_chargable_banks'
    }
    response = call_put_api('resources', request_data)
    return response
  end

  def card_details(card_number)
    request_data = {
      'inquire' => 'card',
      'card_no' => card_number
    }
    response = call_put_api('resources', request_data)
    return response
  end

  def card_charges(card_no, amount)
    request_data = {
      'inquire' => 'charges',
      'card_no' => card_no,
      'amount' => amount
    }
    response = call_put_api('resources', request_data)
    return response
  end

  def account_charges(amount)
    request_data = {
      'inquire' => 'charges',
      'type' => 'account',
      'amount' => amount
    }
    response = call_put_api('resources', request_data)
    return response
  end

  def validate_otp(txn_ref, otp)
    request_data = {
      'action' => 'validate',
      'txnRef' => txn_ref,
      'otp' => otp
    }

    result = call_put_api('payment', request_data)
    return result
  end

  def verify_transaction(txn_ref)
    request_data = {
      'action' => 'verify',
      'txnRef' => txn_ref
    }

    result = call_put_api('payment', request_data)

    return result
  end

  def money_transfer(amount, bankcode, account_number, sender_name, narration)
    request_data = {
      'action' => 'transfer',
      'amount' => amount,
      'bankcode' => bankcode,
      'accountnumber' => account_number,
      'sender_name' => sender_name,
      'narration' => narration
    }
    response = call_put_api('disburse', request_data)
    return response
  end

  def verify_money_transfer(txn_ref)
    request_data = {
      'action' => 'verify',
      'txnRef' => txn_ref
    }
    response = call_put_api('disburse', request_data)
    return response
  end

  def verify_account_name(bankcode, account_number)
    request_data = {
      'action' => 'accountname',
      'bankcode' => bankcode,
      'accountnumber' => account_number
    }
    response = call_put_api('resources', request_data)
    return response
  end

  def call_put_api(api_method, data = {})
    result = nil
    begin
      jdata = JSON.generate(data)
      base_url_with_method = current_base_url + '/' + api_method
      response = RestClient.put base_url_with_method, jdata, content_type: :json, accept: :json, key: @merchant_key, mid: @merchant_id

      unless response.code == 200 || response.code == 201
        raise GladepayServerError.new(response), 'HTTP Code ' + response.code.to_s + ': ' + response.body.to_s
      end

      result = JSON.parse(response.body)
      # puts 'CALL_PUT_API:-RESULT ', result

      unless (result.instance_of? Integer) != false
        raise GladepayServerError.new(response), 'Server Message: '. result['message'].to_s unless result['status'] != 0
      end
    rescue JSON::ParserError => jsonerr
      raise GladepayServerError.new(response), 'Invalid result data. Could not parse JSON response body \n' + jsonerr.message
    rescue GladepayServerError => e
      Utils.server_error_handler(e)
    end

    return result
  end
end
